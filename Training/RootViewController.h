//
//  RootViewController.h
//  Training
//
//  Created by Joan Barrull on 15/02/2020.
//  Copyright © 2020 Joan Barrull. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RootViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
