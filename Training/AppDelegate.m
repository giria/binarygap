//
//  AppDelegate.m
//  Training
//
//  Created by Joan Barrull on 15/02/2020.
//  Copyright © 2020 Joan Barrull. All rights reserved.
//

#import "AppDelegate.h"
#import "RootViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _window.rootViewController = [[RootViewController alloc] initWithNibName:@"RootViewController" bundle:nil];
    
    [_window makeKeyAndVisible];
    
    
    NSString * binary = [self binaryStringWithInteger:1041];
    NSLog(@" %@",binary);
    NSInteger steps = [self solution: 1041];
    NSLog(@"Steps: %ld",steps);
    return YES;
}



- (NSString *)binaryStringWithInteger:(NSInteger)value
{
    NSMutableString *string = [NSMutableString string];
    while (value)
    {
        [string insertString:(value & 1)? @"1": @"0" atIndex:0];
        value /= 2;
    }
    return string;
}
- (NSInteger) findGap: (NSInteger) n {
    NSInteger steps = 0;
    while ( (n & (n+1) )  != 0) {
        n = n | n >> 1;
        steps++;
        
    }
    return steps;
}



#pragma mark - UISceneSession lifecycle

-(int) solution: (int) N {
    // write your code in Objective-C 2.0
    
    //count trailing zeros
    int nrTrailingZeroes = 0;
    int i = (int)N;
    
    
    while ((i & 1) == 0)
    {
        i = i >> 1;
        nrTrailingZeroes++;
    }
    
    N = N >> nrTrailingZeroes;
    
    
    int steps = 0;
    while ( (N & (N+1)) != 0) {
        
        N = N | N >> 1;
        steps++;
        
    }
    
    return steps;
    

    
}



@end
