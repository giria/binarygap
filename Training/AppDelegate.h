//
//  AppDelegate.h
//  Training
//
//  Created by Joan Barrull on 15/02/2020.
//  Copyright © 2020 Joan Barrull. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow             *window;

@end

